Alpine minimal root filesystem from:

wget http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/x86_64/alpine-minirootfs-3.12.0-x86_64.tar.gz

# Web Server Wordpress:

docker build -t alpine-wp:latest --no-cache wordpress/

docker run -d -p 8080:8080 --name alpi-wp -m 4M alpine-wp:latest

docker exec -it alpi-wp /bin/sh







#Sources:
https://github.com/m4rcu5nl/docker-lighttpd-alpine
